/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.dsgroup.BlinkFire;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.HashBiMap;
import com.google.gson.Gson;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 * @author alessandrov
 */
public class ManageBlinkFire {

    private static String OFFICIAL_SPONSOR = "official"; // Official -> Valori ammessi: [Y = Official, N = Non official]
    private static String TYPE_SPONSOR = "type"; // Type -> Valori ammessi: [Entity, Medias, Players]

    public static JSONObject getBlinkFireData(String typeTabella, String jsonString) throws UnsupportedEncodingException {

        System.out.println("Tipo dati: " + typeTabella);

        SimpleDateFormat sdfInput = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdfOutput = new SimpleDateFormat("yyyyMMdd");

        Date dateToFormat;
        String dateEventValue = "";

        JSONObject response = new JSONObject();
        JSONArray social = new JSONArray();

        try {

            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> map = mapper.readValue(jsonString, Map.class);

            switch (typeTabella) {
                case "audience":

                    List<String> valoriCampi = Arrays.asList("medium", "num_followers");

                    dateEventValue = (String) map.get("day");

                    dateToFormat = sdfInput.parse(dateEventValue);

                    dateEventValue = sdfOutput.format(dateToFormat);

                    ArrayList<Map<String, Object>> mediumsMap = (ArrayList<Map<String, Object>>) map.get("mediums");

                    Iterator iter = mediumsMap.iterator();

                    while (iter.hasNext()) {
                        Map<String, Object> valueArray = (Map<String, Object>) iter.next();

                        String mediums = (String) valueArray.get(valoriCampi.get(0));
                        int num_followers = (int) valueArray.get(valoriCampi.get(1));

                        Map<String, Object> mapValues = new LinkedHashMap<>();
                        mapValues.put(valoriCampi.get(0), mediums);
                        mapValues.put(valoriCampi.get(1), num_followers);
                        mapValues.put(valoriCampi.get(2), dateEventValue);

                        JSONObject jsonObj = new JSONObject();
                        jsonObj.put("social", mapValues);

                        social.put(jsonObj);

                    }

                    response.put("socials", social);

                    System.out.println("json: " + response);

                    break;

                case "engagement":

                    valoriCampi = Arrays.asList("medium", "engagement", "post_count", "dateEvent");

                    ArrayList<Map<String, Object>> daysMap = (ArrayList<Map<String, Object>>) map.get("by_day");

                    Iterator<Map<String, Object>> iterEngagement = daysMap.iterator();

                    while (iterEngagement.hasNext()) {
                        Map<String, Object> valueArray = iterEngagement.next();

                        dateEventValue = (String) valueArray.get("day");

                        dateToFormat = sdfInput.parse(dateEventValue);

                        dateEventValue = sdfOutput.format(dateToFormat);

                        ArrayList<Map<String, String>> mediumMap = (ArrayList<Map<String, String>>) valueArray.get("by_medium");

                        Iterator<Map<String, String>> iterMedium = mediumMap.iterator();

                        while (iterMedium.hasNext()) {

                            Map<String, String> mapValues = new LinkedHashMap<>();

                            Map<String, String> mediumArray = iterMedium.next();

                            mapValues.put(valoriCampi.get(0), mediumArray.get(valoriCampi.get(0)));
                            mapValues.put(valoriCampi.get(1), mediumArray.get(valoriCampi.get(1)));
                            mapValues.put(valoriCampi.get(2), mediumArray.get(valoriCampi.get(2)));

                            mapValues.put(valoriCampi.get(3), dateEventValue);

                            JSONObject jsonObj = new JSONObject();
                            jsonObj.put("social", mapValues);

                            social.put(jsonObj);

                        }

                    }

                    response.put("socials", social);

                    System.out.println("json: " + response);

                    break;

                case "sponsorship":

//                    List<String> valoriObjectMap = Arrays.asList("entity", "medias", "players");
                    List<String> valoriObjectMap = Arrays.asList("entity");

                    List<String> listCampiOutput = Arrays.asList("entity_id", "hashtags", "image_url", "impressions", "mentions", "name", "pictures", "posts", "tags", "total_engagement", "url", "valuation", "videos");
                    List<String> listCampiString = Arrays.asList("entity_id", "name", "image_url", "url");

                    dateEventValue = (String) map.get("start_date");

                    dateToFormat = sdfInput.parse(dateEventValue);

                    dateEventValue = sdfOutput.format(dateToFormat);

                    Map<String, Object> mapSponsors = new LinkedHashMap<>();
                    mapSponsors = (Map<String, Object>) map.get("sponsors");

                    Map<String, Object> mapResponse = new LinkedHashMap<>();

                    Iterator<String> iterSponsorship = valoriObjectMap.iterator();

                    while (iterSponsorship.hasNext()) {
                        String keyListName = iterSponsorship.next();

                        Map<String, Object> mapObjectList = (Map<String, Object>) mapSponsors.get(keyListName);
                        Set<String> keySet = mapObjectList.keySet();

                        Iterator<String> iterListkey = keySet.iterator();
                        while (iterListkey.hasNext()) {
                            String keyOfficial = iterListkey.next(); // valori non_offial, official
                            Map<String, Object> mapObjectListKeyOfficial = (Map<String, Object>) mapObjectList.get(keyOfficial);
                            if (mapObjectListKeyOfficial != null && !mapObjectListKeyOfficial.isEmpty() && mapObjectListKeyOfficial.size() > 0) {
                                ArrayList<Map<String, Object>> listMapResultSponsors = (ArrayList<Map<String, Object>>) mapObjectListKeyOfficial.get("sponsors");
                                Iterator<Map<String, Object>> iListMapResultSponsors = listMapResultSponsors.iterator();
                                while (iListMapResultSponsors.hasNext()) {
                                    Map<String, Object> nextIListMapResultSponsors = iListMapResultSponsors.next();

                                    if (nextIListMapResultSponsors.size() < listCampiOutput.size()) {
                                        Iterator<String> iterListCampiOutput = listCampiOutput.iterator();
                                        while (iterListCampiOutput.hasNext()) {
                                            Object valueOutput;
                                            String nextCampoOutput = iterListCampiOutput.next();
                                            if (!nextIListMapResultSponsors.containsKey(nextCampoOutput)) {
                                                if (listCampiString.contains(nextCampoOutput)) {
                                                    valueOutput = "";
                                                } else {
                                                    valueOutput = 0;
                                                }
                                                nextIListMapResultSponsors.put(nextCampoOutput, valueOutput);
                                            }
                                        }
                                    }

//                                    String nameValue = nextIListMapResultSponsors.get("name").toString();
//                                    nameValue = nameValue.replace("ò", "o");
                                    nextIListMapResultSponsors.put("name", nextIListMapResultSponsors.get("name").toString().replace("Ã²", "ò"));
                                    System.out.println("nextIListMapResultSponsors = " + nextIListMapResultSponsors);
                                    mapResponse.putAll(nextIListMapResultSponsors);
                                    mapResponse.put("url", "url-");
                                    mapResponse.put("image_url", "url-");
                                    mapResponse.put("dateEvent", dateEventValue);
                                    mapResponse.put(OFFICIAL_SPONSOR, (keyOfficial.equals("official") ? "Y" : "N"));
                                    mapResponse.put(TYPE_SPONSOR, keyListName);

                                    JSONObject jsonObj = new JSONObject();
                                    jsonObj.put("sponsor", mapResponse);
                                    social.put(jsonObj);
                                }
                            }
                        }
                    }
                    response.put("sponsors", social);
                    System.out.println("response = " + response);

                    break;

                default:
                    break;
            }

        } catch (java.text.ParseException ex) {
            System.out.println("Errore: " + ex.getMessage());
        } catch (JsonProcessingException ex) {
            System.out.println("Errore: " + ex.getMessage());
        }

        return response;

    }

    public static Map<String, Object> massiveImportCycleScript(int contatore, String jsonString) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();
        int fineCiclo = 0;

        Map<String, Object> resultsParam = new LinkedHashMap<>();

        Map<String, Object> massiveImportMap = mapper.readValue(jsonString, Map.class);

        ArrayList<Map<String, Object>> rowsMap = (ArrayList<Map<String, Object>>) massiveImportMap.get("rows");

        fineCiclo = rowsMap.size();

        Map<String, Object> valueMapContatore = rowsMap.get(contatore);

        contatore++;

        resultsParam.put("startDateVar", valueMapContatore.get("startDateVar"));
        resultsParam.put("endDateVar", valueMapContatore.get("endDateVar"));
        resultsParam.put("typeProcess", valueMapContatore.get("TypeProcess"));
        resultsParam.put("contatore", contatore);
        resultsParam.put("fineCiclo", fineCiclo);

        return resultsParam;

    }

}
