/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package it.dsgroup.scriptGoovy

import java.util.*;
import java.io.InputStream;
import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONObject;
import org.apache.commons.io.IOUtils;
// import com.boomi.execution.ExecutionUtil; // Deve essere decommentato quando riportato su Boomi
import com.google.gson.Gson;
import com.fasterxml.jackson.databind.ObjectMapper;

logger = ExecutionUtil.getBaseLogger();

for( int i = 0; i < dataContext.getDataCount(); i++ ) {
    InputStream is = dataContext.getStream(i);
    Properties props = dataContext.getProperties(i);
    
    //String payload = IOUtils.toString(is);
    /*JSONObject nominativo = new JSONObject(payload);
    */
    
    List<String> campiTabella = Arrays.asList("PersonID", "Budget", "BeneMateriale");
    logger.info("is stamp = " + is);
    ObjectMapper mapper = new ObjectMapper();
        
            Map<String, Object> map = mapper.readValue(is, Map.class);
            
            ArrayList rowsMap = (ArrayList) map.get("rows");
            Iterator iter = rowsMap.iterator();

            Map<String, Map<String, String>> mapRowsFinal = new TreeMap<>();
            Map<String, String> outputValue = new TreeMap<>();
            int contatoreElement = 0;
            while (iter.hasNext()) {
                Map<String, ArrayList> tempMap = (Map<String, ArrayList>) iter.next();
//                System.out.println("tempMap.size() = " + tempMap.size());
//                System.out.println("tempMap = " + tempMap);

                Set<String> keySetTmp = tempMap.keySet();

                Iterator<String> iterKeySet = keySetTmp.iterator();

                while (iterKeySet.hasNext()) {
                    String nomeChiaveSet = iterKeySet.next();
                    ArrayList getValueObjectF = tempMap.get(nomeChiaveSet);

                    if (nomeChiaveSet.equals("f")) {
                        nomeChiaveSet = nomeChiaveSet.replace("f", "budget");// + contatoreElement);
                    }
                    Iterator<Map<String, String>> iterValori = getValueObjectF.iterator();
                    if (!outputValue.isEmpty()) {
                        outputValue = new TreeMap<>();
                    }
                    int contatoreElementValue = 0;
                    while (iterValori.hasNext() && contatoreElementValue < campiTabella.size()) {
                        Map<String, String> nextMap = iterValori.next();
                        if (nextMap != null) {
                            for (Map.Entry<String, String> entry : nextMap.entrySet()) {
                                if (entry.getKey().equals("v")) {
                                    String valoreKey = campiTabella.get(contatoreElementValue);
                                    outputValue.put(valoreKey, entry.getValue());
                                    contatoreElementValue++;
                                }
                            }
                        }
                        else {
                            String valoreKey = campiTabella.get(contatoreElementValue);
                            outputValue.put(valoreKey, "");
                            contatoreElementValue++;
                        }

                    }
                    contatoreElement++;
                    mapRowsFinal.put(nomeChiaveSet, outputValue);
                }
            }

            //System.out.println("mapRowsFinal = " + mapRowsFinal);
            Gson gson = new Gson();
            toJson = gson.toJson(mapRowsFinal);
            
            is = IOUtils.toInputStream(toJson);
    
            logger.info("JsonConvertito = " + toJson);
    
    
    //logger.info("Nominativo " + jsonArray);
    
    dataContext.storeStream(is, props);
}
