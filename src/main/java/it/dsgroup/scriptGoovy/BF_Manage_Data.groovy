package it.dsgroup.scriptGoovy

import java.util.*;
import java.io.*; 
import org.json.CDL; 
import org.json.JSONArray; 
import org.json.JSONObject; 
import org.apache.commons.io.IOUtils; 
import com.boomi.execution.ExecutionUtil; 
import com.google.gson.Gson; 
import com.fasterxml.jackson.databind.ObjectMapper; 
import java.text.SimpleDateFormat; 
import java.math.BigDecimal; 
import java.util.Date; 
import java.sql.Timestamp;

logger = ExecutionUtil.getBaseLogger();

for( int i = 0; i < dataContext.getDataCount(); i++ ) {
    InputStream is = dataContext.getStream(i);
    Properties props = dataContext.getProperties(i);
    
    typeTabella = ExecutionUtil.getDynamicProcessProperty("typeTabella");
    
    SimpleDateFormat sdfInput = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat sdfOutput = new SimpleDateFormat("yyyyMMdd");

    Date dateToFormat;
    String dateEventValue = "";    
    
    JSONObject response = new JSONObject();
    JSONArray social = new JSONArray();

    try {

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.readValue(is, Map.class);

        switch (typeTabella) {
            case "audience":

                List<String> valoriCampi = Arrays.asList("medium", "num_followers", "dateEvent");
                
                dateEventValue = (String) map.get("day");

                dateToFormat = sdfInput.parse(dateEventValue);

                dateEventValue = sdfOutput.format(dateToFormat); 
                
                logger.info("dateEventValue: " + dateEventValue);

                ArrayList<Map<String, Object>> mediumsMap = (ArrayList<Map<String, Object>>) map.get("mediums");

                Iterator iter = mediumsMap.iterator();

                while (iter.hasNext()) {
                    Map<String, Object> valueArray = (Map<String, Object>) iter.next();

                    String mediums = (String) valueArray.get(valoriCampi.get(0));
                    int num_followers = (int) valueArray.get(valoriCampi.get(1));

                    Map<String, Object> mapValues = new LinkedHashMap<>();
                    mapValues.put(valoriCampi.get(0), mediums);
                    mapValues.put(valoriCampi.get(1), num_followers);
                    mapValues.put(valoriCampi.get(2), dateEventValue);

                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("social", mapValues);

                    social.put(jsonObj);

                }

                response.put("socials", social);

                logger.info("json: " + response);

                break;

            case "engagement":

                valoriCampi = Arrays.asList("medium", "engagement", "post_count", "dateEvent");

                ArrayList<Map<String, Object>> daysMap = (ArrayList<Map<String, Object>>) map.get("by_day");

                Iterator<Map<String, Object>> iterEngagement = daysMap.iterator();

                while (iterEngagement.hasNext()) {
                    Map<String, Object> valueArray = iterEngagement.next();

                    dateEventValue = (String) valueArray.get("day");

                    dateToFormat = sdfInput.parse(dateEventValue);

                    dateEventValue = sdfOutput.format(dateToFormat);
                    
                    logger.info("dateEventValue: " + dateEventValue);

                    ArrayList<Map<String, String>> mediumMap = (ArrayList<Map<String, String>>) valueArray.get("by_medium");

                    Iterator<Map<String, String>> iterMedium = mediumMap.iterator();

                    while (iterMedium.hasNext()) {

                        Map<String, String> mapValues = new LinkedHashMap<>();

                        Map<String, String> mediumArray = iterMedium.next();

                        mapValues.put(valoriCampi.get(0), mediumArray.get(valoriCampi.get(0)));
                        mapValues.put(valoriCampi.get(1), mediumArray.get(valoriCampi.get(1)));
                        mapValues.put(valoriCampi.get(2), mediumArray.get(valoriCampi.get(2)));

                        mapValues.put(valoriCampi.get(3), dateEventValue);

                        JSONObject jsonObj = new JSONObject();
                        jsonObj.put("social", mapValues);

                        social.put(jsonObj);

                    }

                }

                response.put("socials", social);

                logger.info("json: " + response);

                break;

            case "sponsorship":

                List<String> valoriObjectMap = Arrays.asList("entity", "medias", "players", "other_entities");
                
                dateEventValue = (String) map.get("start_date");

                dateToFormat = sdfInput.parse(dateEventValue);

                dateEventValue = sdfOutput.format(dateToFormat);                

                Map<String, Object> mapSponsors = new LinkedHashMap<>();
                mapSponsors = (Map<String, Object>) map.get("sponsors");

                Map<String, Object> mapResponse = new LinkedHashMap<>();

                Iterator<String> iterSponsorship = valoriObjectMap.iterator();

                while (iterSponsorship.hasNext()) {
                    String keyListName = iterSponsorship.next();

                    Map<String, Object> mapObjectList = (Map<String, Object>) mapSponsors.get(keyListName);

                    if (mapObjectList != null) {
                        Set<String> keySet = mapObjectList.keySet();

                        Iterator<String> iterListkey = keySet.iterator();
                        while (iterListkey.hasNext()) {
                            String keyOfficial = iterListkey.next(); // valori non_offial, official
                            Map<String, Object> mapObjectListKeyOfficial = (Map<String, Object>) mapObjectList.get(keyOfficial);
                            if (mapObjectListKeyOfficial != null && !mapObjectListKeyOfficial.isEmpty() && mapObjectListKeyOfficial.size() > 0) {
                                ArrayList<Map<String, Object>> listMapResultSponsors = (ArrayList<Map<String, Object>>) mapObjectListKeyOfficial.get("sponsors");
                                Iterator<Map<String, Object>> iListMapResultSponsors = listMapResultSponsors.iterator();
                                while (iListMapResultSponsors.hasNext()) {
                                    Map<String, Object> nextIListMapResultSponsors = iListMapResultSponsors.next();
                                    mapResponse.putAll(nextIListMapResultSponsors);
                                    mapResponse.put("dateEvent", dateEventValue);
//                                mapResponse.put("url", "url-"); --> utilizzato solo in fase di test se i documenti sono troppi
//                                mapResponse.put("image_url", "url-"); --> utilizzato solo in fase di test se i documenti sono troppi
                                    mapResponse.put("official", (keyOfficial.equals("official") ? "Y" : "N"));
                                    mapResponse.put("type", keyListName);
                                    JSONObject jsonObj = new JSONObject();
                                    jsonObj.put("sponsor", mapResponse);
                                    social.put(jsonObj);
                                }
                            }
                        }
                    }
                }
                response.put("sponsors", social);
                logger.info("response = " + response);

                break;

            default:
                break;
        }

    } catch (Exception ex) {
        logger.error("Errore: " + ex.getMessage());
    }    
            
    is = IOUtils.toInputStream(response.toString());    

    dataContext.storeStream(is, props);
}