package it.dsgroup.scriptGoovy

import java.util.*;
import java.io.InputStream;
import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONObject;
import org.apache.commons.io.IOUtils;
import com.boomi.execution.ExecutionUtil;
import com.google.gson.Gson;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.text.SimpleDateFormat;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;

for( int i = 0; i < dataContext.getDataCount(); i++ ) {
    InputStream is = dataContext.getStream(i);
    Properties props = dataContext.getProperties(i);

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    Timestamp ts=new Timestamp(System.currentTimeMillis());
    Date date=new Date(ts.getTime());

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.add(Calendar.DATE,-1);

    String dateFormat = sdf.format(calendar.getTime());

    String day = dateFormat;
    
    ExecutionUtil.setDynamicProcessProperty("day", day, false);

    dataContext.storeStream(is, props);
}