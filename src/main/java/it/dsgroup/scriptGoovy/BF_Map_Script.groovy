import java.util.*;
import java.io.InputStream;
import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONObject;
import org.apache.commons.io.IOUtils;
//import com.boomi.execution.ExecutionUtil;
import com.google.gson.Gson;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.text.SimpleDateFormat;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;

logger = ExecutionUtil.getBaseLogger();

for( int i = 0; i < dataContext.getDataCount(); i++ ) {
    InputStream is = dataContext.getStream(i);
    Properties props = dataContext.getProperties(i);

    List<String> listComparation = Arrays.asList("event_date","event_timestamp","event_name","user_pseudo_id");
    List<String> campiTabella = Arrays.asList("Partner", "Posts", "Pictures","Videos","Mentions","HashTags","Brainded_Content","Total","Estimated_Impressions","Link_Clicks");
    String nomeRoot = "Partners";
    ObjectMapper mapper = new ObjectMapper();

    Map<String, Object> map = mapper.readValue(is, Map.class);

    ArrayList rowsMap = (ArrayList) map.get("rows");
    Iterator iter = rowsMap.iterator();

    Map<String, Map<String, Object>> mapRowsFinal = new TreeMap<>();
    Map<String, Object> outputValue = new TreeMap<>();
    int contatoreElement = 0;
    while (iter.hasNext()) {
        Map<String, ArrayList> tempMap = (Map<String, ArrayList>) iter.next();

        Set<String> keySetTmp = tempMap.keySet();

        Iterator<String> iterKeySet = keySetTmp.iterator();

        while (iterKeySet.hasNext()) {
            String nomeChiaveSet = iterKeySet.next();
            ArrayList getValueObjectF = tempMap.get(nomeChiaveSet);

            if (nomeChiaveSet.equals("f")) {
                nomeChiaveSet = nomeChiaveSet.replace("f", nomeRoot);// + contatoreElement);
            }
            Iterator<Map<String, String>> iterValori = getValueObjectF.iterator();
            if (!outputValue.isEmpty()) {
                outputValue = new TreeMap<>();
            }
            int contatoreElementValue = 0;
            while (iterValori.hasNext() && contatoreElementValue < campiTabella.size()) {
                Map<String, String> nextMap = iterValori.next();
                if (nextMap != null) {
                    for (Map.Entry<String, Object> entry : nextMap.entrySet()) {
                        if (entry.getKey().equals("v")) {
                            String valoreKey = campiTabella.get(contatoreElementValue);
                            outputValue.put(valoreKey, entry.getValue());
                            contatoreElementValue++;
                        }
                    }
                }
                else {
                    String valoreKey = campiTabella.get(contatoreElementValue);
                    outputValue.put(valoreKey, "");
                    contatoreElementValue++;
                }

            }

            //Map<String, Object> tempOutputValue = new TreeMap<>();
            /*
            SimpleDateFormat sdfOrigin = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HHmmss.SSS");
            Iterator<String> iterListComparator = listComparation.iterator();
            while(iterListComparator.hasNext()) {
                String valoreComparator = iterListComparator.next();
                Object valueMap = outputValue.get(valoreComparator);
                if (valoreComparator.equals("event_date")) {
                    Date parseDate = sdfOrigin.parse((String) valueMap);
                    valueMap = sdf.format(parseDate);

                }
                else {
                    if (valoreComparator.equals("event_timestamp")) {
                        if (valueMap != null) {
                            long timestampLong = Long.parseLong((String) valueMap);
                            timestampLong = timestampLong / 1000;
                            Timestamp ts=new Timestamp(timestampLong);
                            Date date=new Date(ts.getTime());
                            String format = sdf.format(date);
                            valueMap = format;
                        }
                    }
                }
                */
            //tempOutputValue.put(valoreComparator, valueMap);
            //}

            contatoreElement++;
            mapRowsFinal.put(nomeChiaveSet, outputValue);
        }
    }

    Gson gson = new Gson();
    toJson = gson.toJson(mapRowsFinal);

    is = IOUtils.toInputStream(toJson);

    dataContext.storeStream(is, props);
}
