package it.dsgroup.scriptGoovy

import java.util.*;
import java.io.InputStream;
import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONObject;
import org.apache.commons.io.IOUtils;
//import com.boomi.execution.ExecutionUtil;
import com.google.gson.Gson;
import com.fasterxml.jackson.databind.ObjectMapper;

logger = ExecutionUtil.getBaseLogger();

for( int i = 0; i < dataContext.getDataCount(); i++ ) {
    InputStream is = dataContext.getStream(i);
    Properties props = dataContext.getProperties(i);

    ObjectMapper mapper = new ObjectMapper();
    Map<String, Object> map = mapper.readValue(is, Map.class);

    Map<String, ArrayList<Map<String,Map<String, String>>>> mapResponseDocument = new LinkedHashMap<>();
    ArrayList columnHeadersMap = (ArrayList) map.get("columnHeaders");
    System.out.println("columnHeaders = " + columnHeadersMap);

    Iterator<Map<String,String>> iter = columnHeadersMap.iterator();
    List<String> listColumnHeader = new ArrayList<String>();
    int contatoreElementList = 0;
    while (iter.hasNext()) {
        Map<String, String> iterMapColumn = iter.next();
        String name = iterMapColumn.get("name");
        name = name.replace("ga:", "");
        listColumnHeader.add(contatoreElementList, name);
        contatoreElementList++;
    }
    System.out.println("listColumnHeader = " + listColumnHeader);

    ArrayList rowsMap = (ArrayList) map.get("rows");
    System.out.println("rows = " + rowsMap);


    Map<String,Map<String, String>> mapTemp = new LinkedHashMap<>();
    Map<String, String> mapTempDocument = new LinkedHashMap<>();
    ArrayList<Map<String,Map<String, String>>> arrayMapValue = new ArrayList<Map<String,Map<String, String>>>();
    if (rowsMap != null) {
        Iterator<ArrayList> iterRowList = rowsMap.iterator();
        while (iterRowList.hasNext()) {
            ArrayList nextList = iterRowList.next();
            System.out.println("nextList = " + nextList);
            Iterator<String> iterNextList = nextList.iterator();
            int contatoreNextList = 0;
            while (iterNextList.hasNext()) {
                String value = iterNextList.next();
                if (value.equals("(not set)")) {
                    value = "null";
                }
                String key = listColumnHeader.get(contatoreNextList);
                if (key.contains("ga:")) {
                    key = key.replace("ga:", "");
                }
                mapTempDocument.put(key, value);
                contatoreNextList++;
            }
            mapTemp.put("row", mapTempDocument);
            arrayMapValue.add(mapTemp);
            mapTempDocument = new LinkedHashMap<>();
            mapTemp = new LinkedHashMap<>();
        }
    }
    else {
        Iterator<String> iterListHeader = listColumnHeader.iterator();
        while (iterListHeader.hasNext()) {
            Map<String, String> totalsForAllResults = (Map<String, String>) map.get("totalsForAllResults");
            String next = iterListHeader.next();
            //next = "ga:" + next;
            Object objValue = totalsForAllResults.get("ga:" + next);
            if (objValue != null) {
                //next = next.replace("ga:", "");
                mapTempDocument.put(next, objValue.toString());
            }
            else {
                //next = next.replace("ga:", "");
                mapTempDocument.put(next, "");
            }

        }
        mapTemp.put("row", mapTempDocument);
        arrayMapValue.add(mapTemp);
    }

    mapResponseDocument.put("rows", arrayMapValue);
    logger.info("mapResponseDocument = " + mapResponseDocument);

    Gson gson = new Gson();
    String toJson = gson.toJson(mapResponseDocument);
    logger.info("toJson = " + toJson);

    is = IOUtils.toInputStream(toJson);

    logger.info("JsonConvertito = " + toJson);



    dataContext.storeStream(is, props);
}