import java.util.*;
import java.io.InputStream;
import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONObject;
import org.apache.commons.io.IOUtils;
//import com.boomi.execution.ExecutionUtil;
import com.google.gson.Gson;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.text.SimpleDateFormat;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;

logger = ExecutionUtil.getBaseLogger();

for( int i = 0; i < dataContext.getDataCount(); i++ ) {
    InputStream is = dataContext.getStream(i);
    Properties props = dataContext.getProperties(i);

    List<String> listComparation = Arrays.asList("event_date","event_timestamp","event_name","user_pseudo_id");
    //List<String> campiTabella = Arrays.asList("event_date","event_timestamp","event_name","event_params_0_key","event_params_0_value_string_value","event_params_0_value_int_value","event_params_0_value_float_value","event_params_0_value_double_value","event_params_1_key","event_params_1_value_string_value","event_params_1_value_int_value","event_params_1_value_float_value","event_params_1_value_double_value","event_params_2_key","event_params_2_value_string_value","event_params_2_value_int_value","event_params_2_value_float_value","event_params_2_value_double_value","event_params_3_key","event_params_3_value_string_value","event_params_3_value_int_value","event_params_3_value_float_value","event_params_3_value_double_value","event_params_4_key","event_params_4_value_string_value","event_params_4_value_int_value","event_params_4_value_float_value","event_params_4_value_double_value","event_params_5_key","event_params_5_value_string_value","event_params_5_value_int_value","event_params_5_value_float_value","event_params_5_value_double_value","event_previous_timestamp","event_value_in_usd","event_bundle_sequence_id","event_server_timestamp_offset","user_id","user_pseudo_id","user_properties_0_key","user_properties_0_value_string_value","user_properties_0_value_int_value","user_properties_0_value_float_value","user_properties_0_value_double_value","user_properties_0_value_set_timestamp_micros","user_properties_1_key","user_properties_1_value_string_value","user_properties_1_value_int_value","user_properties_1_value_float_value","user_properties_1_value_double_value","user_properties_1_value_set_timestamp_micros","user_properties_2_key","user_properties_2_value_string_value","user_properties_2_value_int_value","user_properties_2_value_float_value","user_properties_2_value_double_value","user_properties_2_value_set_timestamp_micros","user_first_touch_timestamp","user_ltv","device_category","device_mobile_brand_name","device_mobile_model_name","device_mobile_marketing_name","device_mobile_os_hardware_model","device_operating_system","device_operating_system_version","device_vendor_id","device_advertising_id","device_language","device_is_limited_ad_tracking","device_time_zone_offset_seconds","device_browser","device_browser_version","device_web_info","geo_continent","geo_country","geo_region","geo_city","geo_sub_continent","geo_metro","app_info_id","app_info_version","app_info_install_store","app_info_firebase_app_id","app_info_install_source","traffic_source_name","traffic_source_medium","traffic_source_source","stream_id","platform","event_dimensions","ecommerce","event_params_6_key","event_params_6_value_string_value","event_params_6_value_int_value","event_params_6_value_float_value","event_params_6_value_double_value","event_params_7_key","event_params_7_value_string_value","event_params_7_value_int_value","event_params_7_value_float_value","event_params_7_value_double_value","event_params_8_key","event_params_8_value_string_value","event_params_8_value_int_value","event_params_8_value_float_value","event_params_8_value_double_value","event_params_9_key","event_params_9_value_string_value","event_params_9_value_int_value","event_params_9_value_float_value","event_params_9_value_double_value","event_params_10_key","event_params_10_value_string_value","event_params_10_value_int_value","event_params_10_value_float_value","event_params_10_value_double_value")
    //List<String> campiTabella = Arrays.asList("event_date","event_name","event_timestamp","user_pseudo_id");
    List<String> campiTabella = Arrays.asList("event_date","event_timestamp","event_name","event_params","event_previous_timestamp","event_value_in_usd","event_bundle_sequence_id","event_server_timestamp_offset","user_id","user_pseudo_id","user_properties","user_first_touch_timestamp","user_ltv","device","geo","app_info","traffic_source","stream_id","platform","event_dimensions","ecommerce","items");
    ObjectMapper mapper = new ObjectMapper();
        
    Map<String, Object> map = mapper.readValue(is, Map.class);
            
    ArrayList rowsMap = (ArrayList) map.get("rows");
    Iterator iter = rowsMap.iterator();

    Map<String, ArrayList<Map<String, Map<String, Object>>>> finalObject = new LinkedHashMap<>();
    ArrayList<Map<String, Map<String, Object>>> finalObjectArray = new ArrayList<>();
    
    Map<String, Map<String, Object>> mapRowsFinal = new TreeMap<>();
    Map<String, String> outputValue = new TreeMap<>();
    int contatoreElement = 0;
    while (iter.hasNext()) {
        Map<String, ArrayList> tempMap = (Map<String, ArrayList>) iter.next();

        Set<String> keySetTmp = tempMap.keySet();

        Iterator<String> iterKeySet = keySetTmp.iterator();
        Timestamp ts1=new Timestamp(System.currentTimeMillis());
        Date today = new Date(ts1.getTime());
        SimpleDateFormat sdfTesting = new SimpleDateFormat("yyyyMMdd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        cal.add(Calendar.DATE, -1);
        String dateConfronto = sdfTesting.format(cal.getTime());

        String nomeChiaveSetRoot = "events";
        while (iterKeySet.hasNext()) {
            String nomeChiaveSet = iterKeySet.next();
            ArrayList getValueObjectF = tempMap.get(nomeChiaveSet);

            if (nomeChiaveSet.equals("f")) {
                nomeChiaveSet = nomeChiaveSet.replace("f", "event");
            }
            Iterator<Map<String, String>> iterValori = getValueObjectF.iterator();
            if (!outputValue.isEmpty()) {
                outputValue = new TreeMap<>();
            }
            int contatoreElementValue = 0;
            Boolean dataTrovata = false;
            while (iterValori.hasNext() && contatoreElementValue < campiTabella.size()) {
                Map<String, String> nextMap = iterValori.next();
                if (nextMap != null) {
                    for (Map.Entry<String, String> entry : nextMap.entrySet()) {
                        if (entry.getKey().equals("v")) {

                            String valoreKey = campiTabella.get(contatoreElementValue);
                            String getValueNextMap = entry.getValue();
                            if (valoreKey.equals("event_date")) {
                                if (getValueNextMap.equals(dateConfronto)) {
                                    outputValue.put(valoreKey, getValueNextMap);
                                    contatoreElementValue++;
                                    dataTrovata = true;
                                }
                            }
                            else {
                                if (dataTrovata) {
                                    outputValue.put(valoreKey, getValueNextMap);
                                    contatoreElementValue++;
                                }
                            }
                        }
                    }
						 
                } else {
                    String valoreKey = campiTabella.get(contatoreElementValue);
                    outputValue.put(valoreKey, "");
                    contatoreElementValue++;
                }

            }
            if (!outputValue.containsKey("user_pseudo_id")) {
                outputValue.put("user_pseudo_id", "");
            }
            else {
                if (!outputValue.containsKey("event_date")) {
                    outputValue.put("event_date", "");
                }
                else if (!outputValue.containsKey("event_name")) {
                    outputValue.put("event_name", "");
                }
                else if (!outputValue.containsKey("event_timestamp")) {
                    outputValue.put("event_timestamp", "");
                }
            }
            if (outputValue.get("event_date") != null && !outputValue.get("event_date").equals("") && !outputValue.get("event_timestamp").equals("")) {
                SimpleDateFormat sdfOrigin = new SimpleDateFormat("yyyyMMdd");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HHmmss.SSS");
                Map<String, Object> tempOutputValue = new TreeMap<>();
                Iterator<String> iterListComparator = listComparation.iterator();
                while (iterListComparator.hasNext()) {
                    String valoreComparator = iterListComparator.next();
                    Object valueMap = outputValue.get(valoreComparator);
                    if (valueMap != null || !valueMap.equals("")) {
                        if (valoreComparator.equals("event_date")) {
                            Date parseDate = sdfOrigin.parse((String) valueMap);
																				   
                            valueMap = sdf.format(parseDate);

                        } else {
                            if (valoreComparator.equals("event_timestamp")) {
                                if (valueMap != null) {			   
                                    long timestampLong = Long.parseLong((String) valueMap);
                                    timestampLong = timestampLong / 1000;
                                    Timestamp ts=new Timestamp(timestampLong);
                                    Date date=new Date(ts.getTime());
                                    String format = sdf.format(date);
                                    valueMap = format;
                                }
                            }
                        }
                    }
                    tempOutputValue.put(valoreComparator, valueMap);
                }

								   
					
                contatoreElement++;
                mapRowsFinal.put(nomeChiaveSet, tempOutputValue);
                finalObjectArray.add(mapRowsFinal);
                mapRowsFinal = new LinkedHashMap<>();
            }
        }
        finalObject.put(nomeChiaveSetRoot, finalObjectArray);
    }

    Gson gson = new Gson();
    toJson = gson.toJson(finalObject);
            
    is = IOUtils.toInputStream(toJson);
    
    dataContext.storeStream(is, props);
}