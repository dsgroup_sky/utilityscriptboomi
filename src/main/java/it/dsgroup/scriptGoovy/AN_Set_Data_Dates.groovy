package it.dsgroup.scriptGoovy

import java.util.*;
import java.io.InputStream;
import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONObject;
import org.apache.commons.io.IOUtils;
//import com.boomi.execution.ExecutionUtil;
import com.google.gson.Gson;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.text.SimpleDateFormat;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;

for( int i = 0; i < dataContext.getDataCount(); i++ ) {
    InputStream is = dataContext.getStream(i);
    Properties props = dataContext.getProperties(i);

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    Timestamp ts=new Timestamp(System.currentTimeMillis());
    Date date=new Date(ts.getTime());

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.add(Calendar.DATE,-1);

    String dateFormat = sdf.format(calendar.getTime());

    String startDateVar = dateFormat;
    String endDateVar = dateFormat;

    Map<String, String> mapTableName = new TreeMap<>();
    mapTableName.put("startDateVar", startDateVar);
    mapTableName.put("endDateVar", endDateVar);

    ExecutionUtil.setDynamicProcessProperty("startDateVar", startDateVar, false);
    ExecutionUtil.setDynamicProcessProperty("endDateVar", endDateVar, false);

    Gson gson = new Gson();
    String toJson = gson.toJson(mapTableName);

    is = IOUtils.toInputStream(toJson);

    dataContext.storeStream(is, props);
}
