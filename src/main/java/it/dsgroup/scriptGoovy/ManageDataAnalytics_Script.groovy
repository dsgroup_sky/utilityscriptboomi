/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package it.dsgroup.scriptGoovy

import java.util.*;
import java.io.InputStream;
import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONObject;
import org.apache.commons.io.IOUtils;
// import com.boomi.execution.ExecutionUtil; // Deve essere decommentato quando riportato su Boomi
import com.google.gson.Gson;
import com.fasterxml.jackson.databind.ObjectMapper;

logger = ExecutionUtil.getBaseLogger();

for( int i = 0; i < dataContext.getDataCount(); i++ ) {
    InputStream is = dataContext.getStream(i);
    Properties props = dataContext.getProperties(i);

    ObjectMapper mapper = new ObjectMapper();
    Map<String, Object> map = mapper.readValue(is, Map.class);
    
    ArrayList columnHeadersMap = (ArrayList) map.get("columnHeaders");
    logger.info("columnHeaders = " + columnHeadersMap);
    
    Iterator<Map<String,String>> iter = columnHeadersMap.iterator();
    List<String> listColumnHeader = new ArrayList<String>();
    int contatoreElementList = 0;
    while (iter.hasNext()) {
        Map<String, String> iterMapColumn = iter.next();
        String name = iterMapColumn.get("name");
        listColumnHeader.add(contatoreElementList, name);
        contatoreElementList++;
    }
    logger.info("listColumnHeader = " + listColumnHeader);
    
    
    ArrayList rowsMap = (ArrayList) map.get("rows");
    logger.info("rowsMap = " + rowsMap);
    
    Map<String, ArrayList<Map<String,Map<String, String>>>> mapResponseDocument = new LinkedHashMap<>();
    Iterator<ArrayList> iterRowList = rowsMap.iterator();
    ArrayList<Map<String,Map<String, String>>> arrayMapValue = new ArrayList<Map<String,Map<String, String>>>();
    Map<String,Map<String, String>> mapTemp = new LinkedHashMap<>();
    while (iterRowList.hasNext()) {
        Map<String, String> mapTempDocument = new LinkedHashMap<>();
        
        ArrayList nextList = iterRowList.next();
        logger.info("nextList = " + nextList);
        Iterator<String> iterNextList = nextList.iterator();
        int contatoreNextList = 0;
        while (iterNextList.hasNext()) {
            String value = iterNextList.next();
            if (value.equals("(not set)")) {
                value = "null";
            }
            String key = listColumnHeader.get(contatoreNextList);
            if (key.contains("ga:")) {
                key = key.replace("ga:", "");
            }
            mapTempDocument.put(key, value);
            contatoreNextList++;
        }
        mapTemp.put("row", mapTempDocument);
        arrayMapValue.add(mapTemp);
        mapTemp = new LinkedHashMap<>();
    }
    mapResponseDocument.put("rows", arrayMapValue);
    logger.info("mapResponseDocument = " + mapResponseDocument);
    
    Gson gson = new Gson();
    String toJson = gson.toJson(mapResponseDocument);
    logger.info("toJson = " + toJson);
    
    is = IOUtils.toInputStream(toJson);

    logger.info("JsonConvertito = " + toJson);



    dataContext.storeStream(is, props);
}
