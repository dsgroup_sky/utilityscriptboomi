package it.dsgroup.scriptGoovy

import java.util.*;
import java.io.InputStream;
import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONObject;
import org.apache.commons.io.IOUtils;
import com.boomi.execution.ExecutionUtil;
import com.google.gson.Gson;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.text.SimpleDateFormat;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;

for( int i = 0; i < dataContext.getDataCount(); i++ ) {
    InputStream is = dataContext.getStream(i);
    Properties props = dataContext.getProperties(i);

    SimpleDateFormat sdfInput = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat sdfOutput = new SimpleDateFormat("yyyy-MM-dd");

    startDateVar = ExecutionUtil.getDynamicProcessProperty("startDateVar");
    endDateVar = ExecutionUtil.getDynamicProcessProperty("endDateVar");

    Date startDateToFormat = sdfInput.parse(startDateVar);
    startDateVar = sdfOutput.format(startDateToFormat);
    
    Date endDateToFormat = sdfInput.parse(endDateVar);
    endDateVar = sdfOutput.format(endDateToFormat);
    
    ExecutionUtil.setDynamicProcessProperty("startDateVar", startDateVar, false);
    ExecutionUtil.setDynamicProcessProperty("endDateVar", endDateVar, false);    

    dataContext.storeStream(is, props);
}