/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.dsgroup.BigQuery;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author matteos
 */
public class ManageConcatDocument {
    
    public static void getConcatDocument(List<InputStream> listInputStream) {
        
        List<Map<String, Object>> listMapInputStream = new ArrayList<>();
        Iterator<InputStream> iterListInputStream = listInputStream.iterator();
        ObjectMapper mapper = new ObjectMapper();
        while(iterListInputStream.hasNext()) {
            try {
                InputStream is = iterListInputStream.next();
                Map<String, Object> map = mapper.readValue(is, Map.class);
                listMapInputStream.add(map);
            } catch (IOException ex) {
                Logger.getLogger(ManageConcatDocument.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
//        Iterator<Map<String, Object>> iter = listMapInputStream.iterator();
//        
//        
//        while (iter.hasNext()) {
//            Map<String, Object> next = iter.next();
//            /*
//            Example next:
//            
//                "nominativi": {
//                        "PersonID" : "",
//                        "LastName" : "",
//                        "FirstName" : "",
//                        "Address" : "",
//                        "City" : ""
//                }
//            
//                "budget": {
//                    "PersonID" : "",
//                    "Budget" : "",
//                    "BeneMateriale" : ""
//                }
//                
//            */
//            
//            
//        }
        
    }
    
}
