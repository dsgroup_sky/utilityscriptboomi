/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.dsgroup.BigQuery;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author matteos
 */
public class ManageMap {

    public static String upperString(String nome) {
        String output = "";

        output = nome.toUpperCase();

        return output;
    }

    public static String conversionJsonString(String jsonString, List<String> campiTabella) {

        String toJson = "";

        List<String> listComparation = Arrays.asList("event_date","event_timestamp","event_name","user_pseudo_id");

        try {
            System.out.println("jsonString = " + jsonString);
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> map = mapper.readValue(jsonString, Map.class);

            ArrayList rowsMap = (ArrayList) map.get("rows");
            Iterator iter = rowsMap.iterator();
            Map<String, ArrayList<Map<String, Map<String, Object>>>> finalObject = new LinkedHashMap<>();
            ArrayList<Map<String, Map<String, Object>>> finalObjectArray = new ArrayList<>();
            Map<String, Map<String, Object>> mapRowsFinal = new TreeMap<>();
            Map<String, Object> outputValue = new TreeMap<>();
            int contatoreElement = 0;
            while (iter.hasNext()) {
                Map<String, ArrayList> tempMap = (Map<String, ArrayList>) iter.next();
//                System.out.println("tempMap.size() = " + tempMap.size());
//                System.out.println("tempMap = " + tempMap);

                Set<String> keySetTmp = tempMap.keySet();

                Iterator<String> iterKeySet = keySetTmp.iterator();
                //String dateConfronto = manageDataExtract();
                Date today = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                Calendar cal = Calendar.getInstance();
                cal.setTime(today);
                cal.add(Calendar.DATE, -1);
                String dateConfronto = sdf.format(cal.getTime());
                dateConfronto = "20210517";
                String nomeChiaveSetRoot = "events";

                while (iterKeySet.hasNext()) {
                    String nomeChiaveSet = iterKeySet.next();
                    ArrayList getValueObjectF = tempMap.get(nomeChiaveSet);

                    if (nomeChiaveSet.equals("f")) {
//                        nomeChiaveSet = nomeChiaveSet.replace("f", "ROW_" + contatoreElement);
                        nomeChiaveSet = nomeChiaveSet.replace("f", "event");
                    }
                    Iterator<Map<String, Object>> iterValori = getValueObjectF.iterator();
                    if (!outputValue.isEmpty()) {
                        outputValue = new LinkedHashMap<>();
                    }
                    int contatoreElementValue = 0;
                    Boolean dataTrovata = false;
                    while (iterValori.hasNext() && contatoreElementValue < campiTabella.size()) {
                        Map<String, Object> nextMap = iterValori.next();
                        if (nextMap != null) {
                            for (Map.Entry<String, Object> entry : nextMap.entrySet()) {
                                if (entry.getKey().equals("v")) {

                                    String valoreKey = campiTabella.get(contatoreElementValue);
                                    Object getValueNextMap = entry.getValue();
                                    if (valoreKey.equals("event_date")) {
                                        if (getValueNextMap.equals(dateConfronto)) {
                                            outputValue.put(valoreKey, getValueNextMap);
                                            contatoreElementValue++;
                                            dataTrovata = true;
                                        }
                                    }
                                    else {
                                        if (dataTrovata) {
                                            outputValue.put(valoreKey, getValueNextMap);
                                            contatoreElementValue++;
                                        }
                                    }
                                }
                            }
                        } else {
                          String valoreKey = campiTabella.get(contatoreElementValue);
                          outputValue.put(valoreKey, "");
                          contatoreElementValue++;
                        }
                    
                    }
                    if (!outputValue.containsKey("user_pseudo_id")) {
                        outputValue.put("user_pseudo_id", "");
                    }
                    else {
                        if (!outputValue.containsKey("event_date")) {
                            outputValue.put("event_date", "");
                        }
                        else if (!outputValue.containsKey("event_name")) {
                            outputValue.put("event_name", "");
                        }
                        else if (!outputValue.containsKey("event_timestamp")) {
                            outputValue.put("event_timestamp", "");
                        }
                    }
                    System.out.println("print Map=" + outputValue);
                    if (outputValue.get("event_date") != null && !outputValue.get("event_date").equals("") && !outputValue.get("event_timestamp").equals("")) {
                        SimpleDateFormat sdfOrigin = new SimpleDateFormat("yyyyMMdd");
                        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd HHmmss.SSS");
                        Map<String, Object> tempOutputValue = new TreeMap<>();
                        Iterator<String> iterListComparator = listComparation.iterator();
                        while (iterListComparator.hasNext()) {
                            String valoreComparator = iterListComparator.next();
                            Object valueMap = outputValue.get(valoreComparator);
                            System.out.println("valueMap = " + valueMap);
                            if (valueMap != null) {
                                if (valoreComparator.equals("event_date")) {
                                    Date parseDate = sdfOrigin.parse((String) valueMap);
                                    System.out.println("parseDate = " + parseDate);
                                    valueMap = sdf1.parse(sdf1.format(parseDate));

                                } else {
                                    if (valoreComparator.equals("event_timestamp")) {
                                        long timestampLong = Long.parseLong((String) valueMap);

                                        valueMap = new Date(timestampLong / 1000L);
                                    }
                                }
                            }
                            tempOutputValue.put(valoreComparator, valueMap);
                        }

                        contatoreElement++;
                        mapRowsFinal.put(nomeChiaveSet, tempOutputValue);
                        finalObjectArray.add(mapRowsFinal);
                        mapRowsFinal = new LinkedHashMap<>();
                    }
                }
                finalObject.put(nomeChiaveSetRoot, finalObjectArray);
            }

            //System.out.println("mapRowsFinal = " + mapRowsFinal);
            Gson gson = new Gson();
            toJson = gson.toJson(finalObject);

        } catch (JsonProcessingException ex) {
            Logger.getLogger(ManageMap.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return toJson;
    }

    public static String conversionJsonString(JSONObject jsonObject, List<String> campiTabella) {

        String jsonString = jsonObject.toString();
        String conversionJsonString = conversionJsonString(jsonString, campiTabella);

        return conversionJsonString;

    }

    public static void conversionJsonString_v2(String jsonString, List<String> campiTabella) {

        String toJson = "";

        JSONObject jsonObject = new JSONObject(jsonString);

        JSONArray arrayJson = (JSONArray) jsonObject.get("rows");
        Iterator<Object> iter = arrayJson.iterator();
        int contatoreElementValue = 0;
        Map<String, String> outputValue = new HashMap<>();
        while (iter.hasNext()) {
            Map<String, String> nextValue = (Map<String, String>) iter.next();
            for (Map.Entry<String, String> entry : nextValue.entrySet()) {
                if (entry.getKey().equals("v")) {
                    String valoreKey = campiTabella.get(contatoreElementValue);
                    outputValue.put(valoreKey, entry.getValue());
                    contatoreElementValue++;
                }
            }

        }
        System.out.println("outputValue = " + outputValue);

//        return toJson;
    }

    private static String manageDataExtract() {
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        cal.add(Calendar.DATE, -1);
        String dateFormat = sdf.format(cal.getTime());
        System.out.println("dateFormat = " + dateFormat);
        return dateFormat;
    }

}
