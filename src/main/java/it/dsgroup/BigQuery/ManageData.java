/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.dsgroup.BigQuery;

//import com.boomi.execution.ExecutionUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
//import com.boomi.execution.*;

/**
 *
 * @author alessandrov
 */
public class ManageData {

    public static Map<String, Map<String, Object>> defineBQDates(String startDateVar, String endDateVar, int contatore, Map<String, Map<String, Object>> mapResultsComplete) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Map<String, Object> mapResults = new LinkedHashMap<>();
                
        String dateStart = "";
        String dateEnd = "";

        if (contatore == 0) {
            dateStart = startDateVar;
            dateEnd = endDateVar;
        }
        else {
            
            mapResults = mapResultsComplete.get("eventMap");
            dateStart = (String) mapResults.get("startDateVar");
            dateEnd = (String) mapResults.get("endDateVar");
        }
        
        Date sdfStartDate = sdf.parse(dateStart);
        Date sdfEndDate = sdf.parse(dateEnd);
        
        Long numeroGiorni = (sdfEndDate.getTime() - sdfStartDate.getTime()) / 86400000L;

        Boolean executeProcess = true;

        String dateEvent = ""; // Dynamic Process Property
        String nomeTabella = ""; // Dynamic Process Property

        if (contatore == 0) {
            dateEvent = dateStart;
            contatore++;
        } else if (contatore > 0 && contatore <= numeroGiorni) {
            Calendar c = Calendar.getInstance();
            c.setTime(sdfStartDate);
            c.add(Calendar.DATE, contatore);
            dateEvent = sdf.format(c.getTime());

            contatore++;
        } else {
            executeProcess = false;
        }

        if (executeProcess == false) {
            nomeTabella = "";
        } else {
            nomeTabella = "events_" + dateEvent;
        }
        
        

        mapResults.put("startDateVar", dateStart);
        mapResults.put("endDateVar",dateEnd);
        
        mapResults.put("contatore", String.valueOf(contatore));
        mapResults.put("nomeTabella", nomeTabella);
        mapResults.put("executeProcess", String.valueOf(executeProcess));
        
        Map<String, Map<String, Object>> eventDataOutput = new LinkedHashMap<>();
        
        eventDataOutput.put("eventMap", mapResults);

        return eventDataOutput;

    }
        
}
