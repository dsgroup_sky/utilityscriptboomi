/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.dsgroup.analytics;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author matteos
 */
public class ManageDataAnalytics {

    public static void readDocumentAndJsonFormat(String jsonString) {

        try {
            System.out.println("jsonString = " + jsonString);
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> map = mapper.readValue(jsonString, Map.class);
            
            ArrayList columnHeadersMap = (ArrayList) map.get("columnHeaders");
            System.out.println("columnHeaders = " + columnHeadersMap);
            
            Iterator<Map<String,String>> iter = columnHeadersMap.iterator();
            List<String> listColumnHeader = new ArrayList<String>();
            int contatoreElementList = 0;
            while (iter.hasNext()) {
                Map<String, String> iterMapColumn = iter.next();
                String name = iterMapColumn.get("name");
                listColumnHeader.add(contatoreElementList, name);
                contatoreElementList++;
            }
            System.out.println("listColumnHeader = " + listColumnHeader);
            
            
            ArrayList rowsMap = (ArrayList) map.get("rows");
            System.out.println("rowsMap = " + rowsMap);
            
            Map<String, ArrayList<Map<String,Map<String, String>>>> mapResponseDocument = new LinkedHashMap<>();
            Iterator<ArrayList> iterRowList = rowsMap.iterator();
            ArrayList<Map<String,Map<String, String>>> arrayMapValue = new ArrayList<Map<String,Map<String, String>>>();
            Map<String,Map<String, String>> mapTemp = new LinkedHashMap<>();
            while (iterRowList.hasNext()) {
                Map<String, String> mapTempDocument = new LinkedHashMap<>();
                
                ArrayList nextList = iterRowList.next();
                System.out.println("nextList = " + nextList);
                Iterator<String> iterNextList = nextList.iterator();
                int contatoreNextList = 0;
                while (iterNextList.hasNext()) {
                    String value = iterNextList.next();
                    if (value.equals("(not set)")) {
                        value = "null";
                    }
                    String key = listColumnHeader.get(contatoreNextList);
                    if (key.contains("ga:")) {
                        key = key.replace("ga:", "");
                    }
                    mapTempDocument.put(key, value);
                    contatoreNextList++;
                }
                mapTemp.put("row", mapTempDocument);
                arrayMapValue.add(mapTemp);
                mapTemp = new LinkedHashMap<>();
            }
            mapResponseDocument.put("rows", arrayMapValue);
            System.out.println("mapResponseDocument = " + mapResponseDocument);
            
            Gson gson = new Gson();
            String toJson = gson.toJson(mapResponseDocument);
            System.out.println("toJson = " + toJson);
            
        } catch (JsonProcessingException ex) {
            Logger.getLogger(ManageDataAnalytics.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void createDate() throws ParseException {

        Date date = new Date();
        System.out.println("dateFormat = " + date);
        String pattern = "YYYY-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String dateString = simpleDateFormat.format(date);
        System.out.println("dateFormat = " + dateString);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE,-1);
        String dateFormat = simpleDateFormat.format(calendar.getTime());
        System.out.println("dateFormat = " + dateFormat);

    }

    public static void setDataDate() throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Timestamp ts=new Timestamp(System.currentTimeMillis());
        Date date=new Date(ts.getTime());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE,-1);

        String dateFormat = sdf.format(calendar.getTime());

        String startDateVar = dateFormat;
        String endDateVar = dateFormat;

        Map<String, String> mapTableName = new TreeMap<>();
        mapTableName.put("startDateVar", startDateVar);
        mapTableName.put("endDateVar", endDateVar);

        Gson gson = new Gson();
        String toJson = gson.toJson(mapTableName);

        System.out.println("Json: " + toJson);

    }

    public static Map<String, ArrayList<Map<String,Map<String, String>>>>  gestioneDataImpressions(String jsonString) {
        Map<String, ArrayList<Map<String,Map<String, String>>>> mapResponseDocument = new LinkedHashMap<>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> map = mapper.readValue(jsonString, Map.class);

            ArrayList columnHeadersMap = (ArrayList) map.get("columnHeaders");
            System.out.println("columnHeaders = " + columnHeadersMap);

            Iterator<Map<String,String>> iter = columnHeadersMap.iterator();
            List<String> listColumnHeader = new ArrayList<String>();
            int contatoreElementList = 0;
            while (iter.hasNext()) {
                Map<String, String> iterMapColumn = iter.next();
                String name = iterMapColumn.get("name");
                name = name.replace("ga:", "");
                listColumnHeader.add(contatoreElementList, name);
                contatoreElementList++;
            }
            System.out.println("listColumnHeader = " + listColumnHeader);

            ArrayList rowsMap = (ArrayList) map.get("rows");
            System.out.println("rows = " + rowsMap);


            Map<String,Map<String, String>> mapTemp = new LinkedHashMap<>();
            Map<String, String> mapTempDocument = new LinkedHashMap<>();
            ArrayList<Map<String,Map<String, String>>> arrayMapValue = new ArrayList<Map<String,Map<String, String>>>();
            if (rowsMap != null) {
                Iterator<ArrayList> iterRowList = rowsMap.iterator();
                while (iterRowList.hasNext()) {
                    ArrayList nextList = iterRowList.next();
                    System.out.println("nextList = " + nextList);
                    Iterator<String> iterNextList = nextList.iterator();
                    int contatoreNextList = 0;
                    while (iterNextList.hasNext()) {
                        String value = iterNextList.next();
                        if (value.equals("(not set)")) {
                            value = "null";
                        }
                        String key = listColumnHeader.get(contatoreNextList);
                        if (key.contains("ga:")) {
                            key = key.replace("ga:", "");
                        }
                        mapTempDocument.put(key, value);
                        contatoreNextList++;
                    }
                    mapTemp.put("row", mapTempDocument);
                    arrayMapValue.add(mapTemp);
                    mapTempDocument = new LinkedHashMap<>();
                    mapTemp = new LinkedHashMap<>();
                }
            }
            else {
                Iterator<String> iterListHeader = listColumnHeader.iterator();
                while (iterListHeader.hasNext()) {
                    Map<String, String> totalsForAllResults = (Map<String, String>) map.get("totalsForAllResults");
                    String next = iterListHeader.next();
                    //next = "ga:" + next;
                    Object objValue = totalsForAllResults.get("ga:" + next);
                    if (objValue != null) {
                        //next = next.replace("ga:", "");
                        mapTempDocument.put(next, objValue.toString());
                    }
                    else {
                        //next = next.replace("ga:", "");
                        mapTempDocument.put(next, "");
                    }

                }
                mapTemp.put("row", mapTempDocument);
                arrayMapValue.add(mapTemp);
            }

            mapResponseDocument.put("rows", arrayMapValue);


        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return mapResponseDocument;
    }
   
}
