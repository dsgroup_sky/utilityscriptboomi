/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.dsgroup.blinkfire;

import com.fasterxml.jackson.core.JsonProcessingException;
import it.dsgroup.BigQuery.ManageData;
import it.dsgroup.BigQuery.ManageMap;
import it.dsgroup.BlinkFire.ManageBlinkFire;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author alessandrov
 */
public class TestCaseBlinkFire {

    @Test
    public void testManageBlinkFireJson() throws IOException, ParseException {

//        String typeTabella = "audience"; // ["audience", "engagement", "sponsorship"]
//        String typeTabella = "engagement"; // ["audience", "engagement", "sponsorship"]
        String typeTabella = "sponsorship"; // ["audience", "engagement", "sponsorship"]

        JSONParser parser = new JSONParser();

        // Audience
//        Object obj = parser.parse(new FileReader("C:\\Sviluppo\\Dell Boomi\\master\\src\\main\\resources\\testingFile\\01-audiences_response.json"));
//        Object obj = parser.parse(new FileReader("C:\\Sviluppo\\Dell Boomi\\master\\src\\main\\resources\\testingFile\\02-reports_daily_engagement_response.json"));
        Object obj = parser.parse(new FileReader("./src/main/resources/testingFile/03-reports_sponsorship_response.json"));
        
        
        final File initialFile = new File("./src/main/resources/testingFile/bf_sponsorship_response_04052021.json");
        final InputStream targetStream = new DataInputStream(new FileInputStream(initialFile));

        
        
        
        JSONObject jsonObj = (JSONObject) obj;

        String jsonString = jsonObj.toString();

        JSONObject blinkFireData = ManageBlinkFire.getBlinkFireData(typeTabella, jsonString);
        System.out.println("blinkFireData = " + blinkFireData);
//        PrintWriter file = new PrintWriter("./src/main/resources/testingFile/Response.json");
//        file.write(blinkFireData.toJSONString());

    }

    @Test
    public void testMassiveImportCycleScript() throws FileNotFoundException, JsonProcessingException, IOException, ParseException {
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader("./src/main/resources/testingFile/04-set-parameter-massive-sponsorship.json"));
        JSONObject jsonObj = (JSONObject) obj;
        String jsonString = jsonObj.toString();
        int contatore = 0;
        int fineCiclo;
        do {
            Map<String, Object> massiveImportCycleScript = ManageBlinkFire.massiveImportCycleScript(contatore, jsonString);

            contatore = Integer.parseInt(String.valueOf(massiveImportCycleScript.get("contatore")));
            fineCiclo = Integer.parseInt(String.valueOf(massiveImportCycleScript.get("fineCiclo")));

            System.out.println("startDateVar = " + massiveImportCycleScript.get("startDateVar") + "\nendDateVar = " + massiveImportCycleScript.get("endDateVar") + "\ntypeProcess = " + massiveImportCycleScript.get("typeProcess"));

        } while (contatore < fineCiclo);

    }

    @Test
    public void testParamInput() {

        Map<String, String> mapParamInput = new LinkedHashMap<>();

        mapParamInput.put("documentParam_audience", "2021-05-01;2021-05-03;audience");
        mapParamInput.put("documentParam_sponsorship", "2021-04-01;2021-04-03;sponsorship");
        mapParamInput.put("documentParam_engagement", "2021-05-05;2021-05-12;engagement");
        String elementi = "audience;sponsorship;engagement";
        String[] splitElementi = elementi.split(";");
        int fineCiclo = splitElementi.length;
        int contatore = 0;
        while (contatore < fineCiclo) {
            String valueSplitElementi = splitElementi[contatore];

            valueSplitElementi = "documentParam_" + valueSplitElementi;

            System.out.println("valueSplitElementi = " + valueSplitElementi);

            String valueDocument = mapParamInput.get(valueSplitElementi);

            String[] splitValueDocument = valueDocument.split(";");
            System.out.println("startDateVar = " + splitValueDocument[0]);
            System.out.println("endDateVar = " + splitValueDocument[1]);
            System.out.println("typeProcess = " + splitValueDocument[2]);
            contatore++;
        }

    }
    
    
    @Test
    public void testDiffDate() throws java.text.ParseException {
        
        String startDate = "2021-05-01";
        String endDate = "2021-06-02";
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        Date startDateParse = sdf.parse(startDate);
        Date endDateParse = sdf.parse(endDate);
        
        int diffDate = (int) ((endDateParse.getTime() - startDateParse.getTime()) / 86400000);
        System.out.println("diffDate = " + diffDate);
    }
}
