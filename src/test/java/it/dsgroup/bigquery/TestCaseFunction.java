/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.dsgroup.bigquery;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.dsgroup.BigQuery.ManageData;
import it.dsgroup.BigQuery.ManageMap;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.json.simple.JSONObject;

import org.json.simple.parser.JSONParser;

import org.junit.Test;

/**
 * @author matteos
 */
public class TestCaseFunction {

    //    @Test
//    public void testManageMap() {
//        String input = "ciao";
//        
//        input = ManageMap.upperString(input);
//        
//        System.out.println("Valore convertito=" + input);
//    
//    }
    /*
    "{\"kind\":\"bigquery#tableDataList\",\"etag\":\"KxaUr+vlJDld6dhTBWJ\/Hw==\",\"totalRows\":\"3\",\"rows\":[{\"f\":[{\"v\":\"\"},{\"v\":\"\"},{\"v\":\"\"},{\"v\":\"\"},{\"v\":\"\"}]},{\"f\":[{\"v\":\"\"},{\"v\":\"\"},{\"v\":\"\"},{\"v\":\"\"},{\"v\":\"\"}]}]}"
     */
    @Test
    public void testManageJson() throws JsonProcessingException {
//        String jsonString = "{\"kind\":\"bigquery#tableDataList\",\"etag\":\"KxaUr+vlJDld6dhTBWJ\\/Hw==\",\"totalRows\":\"3\",\"rows\":[{\"f\":[{\"v\":\"\"},{\"v\":\"\"},{\"v\":\"\"},{\"v\":\"\"},{\"v\":\"\"}]}]}";
        String jsonString = "{\"kind\":\"bigquery#tableDataList\",\"etag\":\"KxaUr+vlJDld6dhTBWJ\\/Hw==\",\"totalRows\":\"3\",\"rows\":[{\"f\":[{\"v\":\"\"},{\"v\":\"\"},{\"v\":\"\"},null,null]}]}";
//        String jsonString = "{\"kind\":\"bigquery#tableDataList\",\"etag\":\"KxaUr+vlJDld6dhTBWJ\\/Hw==\",\"totalRows\":\"3\",\"rows\":[{\"f\":[{\"v\":\"\"},{\"v\":\"\"},{\"v\":\"\"},{\"v\":\"\"},{\"v\":\"\"}]},{\"f\":[{\"v\":\"\"},{\"v\":\"\"},{\"v\":\"\"},{\"v\":\"\"},{\"v\":\"\"}]}]}";
//            String jsonString = "{\n" +
//"  \"kind\" : \"bigquery#tableDataList\",\n" +
//"  \"etag\" : \"KxaUr+vlJDld6dhTBWJ/Hw==\",\n" +
//"  \"totalRows\" : \"3\",\n" +
//"  \"rows\" : [\n" +
//"    {\n" +
//"      \"f\" : [\n" +
//"        {\n" +
//"          \"v\" : \"Matteo\"\n" +
//"        },\n" +
//"        {\n" +
//"          \"v\" : \"\"\n" +
//"        },\n" +
//"        {\n" +
//"          \"v\" : \"\"\n" +
//"        },\n" +
//"        {\n" +
//"          \"v\" : \"\"\n" +
//"        },\n" +
//"        {\n" +
//"          \"v\" : \"\"\n" +
//"        }\n" +
//"      ]\n" +
//"    }\n" +
//"  ]\n" +
//"}";

        List<String> CAMPI_TABELLA_NOMINATIVI = Arrays.asList("PersonId", "FirstName", "LastName", "Address", "City");
        List<String> CAMPI_TABELLA_BUDGET = Arrays.asList("PersonId", "Budget", "BeneMateriale");
        String conversionJsonString = ManageMap.conversionJsonString(jsonString, CAMPI_TABELLA_NOMINATIVI);
        System.out.println("conversionJsonString = " + conversionJsonString);

//        JSONObject jsonObject = new JSONObject(jsonString);
//        String conversionJsonString = ManageMap.conversionJsonString(jsonObject, CAMPI_TABELLA_BUDGET);
//        System.out.println("conversionJsonString_2 = " + conversionJsonString);
//        
    }



    @Test
    public void testConversionDate() throws ParseException {
        String dateString = "20210430";

        Date dateConver = new SimpleDateFormat("yyyyMMdd").parse(dateString);
        System.out.println("dateConver = " + dateConver);

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd HHmmss.SSS");
        String dateConversione = sdf2.format(dateConver);
        System.out.println("dateConversione = " + dateConversione);
    }

    @Test
    public void testConversionTimeStamp() throws ParseException {
        String dateString = "1617626175910004";
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd HHmmss.SSS");
        long timestampLong = Long.parseLong(dateString);

        System.out.println("timestampString = " + timestampLong);

        Date date1 = new Date(timestampLong / 1000L);

        //timestampLong = timestampLong / 1000L;
        String format = sdf2.format(date1);


        System.out.println("#####" + sdf2.parse(format));

    }

    
    @Test
    public void testManageBQ() throws FileNotFoundException, IOException, org.json.simple.parser.ParseException {
        JSONParser parser = new JSONParser();

        Object obj = parser.parse(new FileReader("C:\\Users\\matteos\\Documents\\DS_GROUP\\ACMilan\\ProjectIntegration\\utilityscriptboomi\\src\\main\\resources\\testingFile\\response20210517.json"));

        JSONObject jsonObj = (JSONObject) obj;
        
        String jsonString = jsonObj.toString();

//        List<String> CAMPI_TABELLA_NOMINATIVI = Arrays.asList("event_date", "event_timestamp", "event_name", "event_params_0_key", "event_params_0_value_string_value", "event_params_0_value_int_value", "event_params_0_value_float_value", "event_params_0_value_double_value", "event_params_1_key", "event_params_1_value_string_value", "event_params_1_value_int_value", "event_params_1_value_float_value", "event_params_1_value_double_value", "event_params_2_key", "event_params_2_value_string_value", "event_params_2_value_int_value", "event_params_2_value_float_value", "event_params_2_value_double_value", "event_params_3_key", "event_params_3_value_string_value", "event_params_3_value_int_value", "event_params_3_value_float_value", "event_params_3_value_double_value", "event_params_4_key", "event_params_4_value_string_value", "event_params_4_value_int_value", "event_params_4_value_float_value", "event_params_4_value_double_value", "event_params_5_key", "event_params_5_value_string_value", "event_params_5_value_int_value", "event_params_5_value_float_value", "event_params_5_value_double_value", "event_previous_timestamp", "event_value_in_usd", "event_bundle_sequence_id", "event_server_timestamp_offset", "user_id", "user_pseudo_id", "user_properties_0_key", "user_properties_0_value_string_value", "user_properties_0_value_int_value", "user_properties_0_value_float_value", "user_properties_0_value_double_value", "user_properties_0_value_set_timestamp_micros", "user_properties_1_key", "user_properties_1_value_string_value", "user_properties_1_value_int_value", "user_properties_1_value_float_value", "user_properties_1_value_double_value", "user_properties_1_value_set_timestamp_micros", "user_properties_2_key", "user_properties_2_value_string_value", "user_properties_2_value_int_value", "user_properties_2_value_float_value", "user_properties_2_value_double_value", "user_properties_2_value_set_timestamp_micros", "user_first_touch_timestamp", "user_ltv", "device_category", "device_mobile_brand_name", "device_mobile_model_name", "device_mobile_marketing_name", "device_mobile_os_hardware_model", "device_operating_system", "device_operating_system_version", "device_vendor_id", "device_advertising_id", "device_language", "device_is_limited_ad_tracking", "device_time_zone_offset_seconds", "device_browser", "device_browser_version", "device_web_info", "geo_continent", "geo_country", "geo_region", "geo_city", "geo_sub_continent", "geo_metro", "app_info_id", "app_info_version", "app_info_install_store", "app_info_firebase_app_id", "app_info_install_source", "traffic_source_name", "traffic_source_medium", "traffic_source_source", "stream_id", "platform", "event_dimensions", "ecommerce", "event_params_6_key", "event_params_6_value_string_value", "event_params_6_value_int_value", "event_params_6_value_float_value", "event_params_6_value_double_value", "event_params_7_key", "event_params_7_value_string_value", "event_params_7_value_int_value", "event_params_7_value_float_value", "event_params_7_value_double_value", "event_params_8_key", "event_params_8_value_string_value", "event_params_8_value_int_value", "event_params_8_value_float_value", "event_params_8_value_double_value", "event_params_9_key", "event_params_9_value_string_value", "event_params_9_value_int_value", "event_params_9_value_float_value", "event_params_9_value_double_value", "event_params_10_key", "event_params_10_value_string_value", "event_params_10_value_int_value", "event_params_10_value_float_value", "event_params_10_value_double_value");
        List<String> CAMPI_TABELLA_NOMINATIVI = Arrays.asList("event_date","event_timestamp","event_name","event_params","event_previous_timestamp","event_value_in_usd","event_bundle_sequence_id","event_server_timestamp_offset","user_id","user_pseudo_id","user_properties","user_first_touch_timestamp","user_ltv","device","geo","app_info","traffic_source","stream_id","platform","event_dimensions","ecommerce","items");
        List<String> CAMPI_TABELLA_BUDGET = Arrays.asList("PersonId", "Budget", "BeneMateriale");
        String conversionJsonString = ManageMap.conversionJsonString(jsonString, CAMPI_TABELLA_NOMINATIVI);
        System.out.println("Conversion JSON string: " + conversionJsonString);

    }

    @Test
    public void testEventsDates() throws ParseException {

        // Input
        String startDateVar = "20210510"; // get del Document Process Property
        String endDateVar = "20210517"; // get del Document Process Property

        int contatore = 0;

        Map<String, Map<String, Object>> output = new LinkedHashMap<>();
        Boolean execBoolean;
        

        do {
            output = ManageData.defineBQDates(startDateVar, endDateVar, contatore,output);

            System.out.println("nomeTabella: " + output.get("eventMap").get("nomeTabella"));
            System.out.println("contatore: " + output.get("eventMap").get("contatore"));
            System.out.println("executeProcess: " + output.get("eventMap").get("executeProcess"));
            
            String contString = (String) output.get("eventMap").get("contatore");
            String execBooleanString = (String) output.get("eventMap").get("executeProcess");
            execBoolean = Boolean.parseBoolean(execBooleanString);

            contatore = Integer.parseInt(contString);

        } while (execBoolean == true);

    }
    
    
    @Test
    public void testInputStream() throws UnsupportedEncodingException, IOException {
        
        String toString = "DBSTART|770df2c6-2793-431d-a45e-7af1bf5db122|2|@|BEGIN|5|@|OUT_START|6|@|20210405 000000.000|^|first_open|^|20210405 193532.178|^||^|20210522 162118.290|#||#|OUT_END|6|@|END|5|@|DBEND|770df2c6-2793-431d-a45e-7af1bf5db122|2|@|DBSTART|770df2c6-2793-431d-a45e-7af1bf5db122|2|@|BEGIN|5|@|OUT_START|6|@|20210408 000000.000|^|first_open|^|20210405 193532.178|^||^|20210522 162118.293|#||#|OUT_END|6|@|END|5|@|DBEND|770df2c6-2793-431d-a45e-7af1bf5db122|2|@|DBSTART|770df2c6-2793-431d-a45e-7af1bf5db122|2|@|BEGIN|5|@|OUT_START|6|@|20210407 000000.000|^|first_open|^|20210405 193532.178|^||^|20210522 162118.293|#||#|OUT_END|6|@|END|5|@|DBEND|770df2c6-2793-431d-a45e-7af1bf5db122|2|@|DBSTART|770df2c6-2793-431d-a45e-7af1bf5db122|2|@|BEGIN|5|@|OUT_START|6|@|20210406 000000.000|^|first_open|^|20210405 193532.178|^||^|20210522 162118.294|#||#|OUT_END|6|@|END|5|@|DBEND|770df2c6-2793-431d-a45e-7af1bf5db122|2|@|DBSTART|770df2c6-2793-431d-a45e-7af1bf5db122|2|@|BEGIN|5|@|OUT_START|6|@|20210409 000000.000|^|first_open|^|20210405 193532.178|^||^|20210522 162118.294|#||#|OUT_END|6|@|END|5|@|DBEND|770df2c6-2793-431d-a45e-7af1bf5db122|2|@|DBSTART|770df2c6-2793-431d-a45e-7af1bf5db122|2|@|BEGIN|5|@|OUT_START|6|@|20210410 000000.000|^|first_open|^|20210405 193532.178|^||^|20210522 162118.294|#||#|OUT_END|6|@|END|5|@|DBEND|770df2c6-2793-431d-a45e-7af1bf5db122|2|@|DBSTART|770df2c6-2793-431d-a45e-7af1bf5db122|2|@|BEGIN|5|@|OUT_START|6|@|20210411 000000.000|^|first_open|^|20210405 193532.178|^||^|20210522 162118.294|#||#|OUT_END|6|@|END|5|@|DBEND|770df2c6-2793-431d-a45e-7af1bf5db122|2|@|DBSTART|770df2c6-2793-431d-a45e-7af1bf5db122|2|@|BEGIN|5|@|OUT_START|6|@|20210412 000000.000|^|first_open|^|20210405 193532.178|^||^|20210522 162118.294|#||#|OUT_END|6|@|END|5|@|DBEND|770df2c6-2793-431d-a45e-7af1bf5db122|2|@|DBSTART|770df2c6-2793-431d-a45e-7af1bf5db122|2|@|BEGIN|5|@|OUT_START|6|@|20210405 000000.000|^|home_content|^|20210405 165717.483|^||^|20210522 162118.294|#||#|OUT_END|6|@|END|5|@|DBEND|770df2c6-2793-431d-a45e-7af1bf5db122|2|@|DBSTART|770df2c6-2793-431d-a45e-7af1bf5db122|2|@|BEGIN|5|@|OUT_START|6|@|20210408 000000.000|^|first_open|^|20210405 165717.483|^||^|20210522 162118.294|#||#|OUT_END|6|@|END|5|@|DBEND|770df2c6-2793-431d-a45e-7af1bf5db122|2|@|";
        
//        InputStream stream = new ByteArrayInputStream(toString.getBytes("UTF-8"));
//        try {
//        ObjectMapper mapper = new ObjectMapper();
//    
//        Object obj = mapper.readValue(stream,Object.class);
//        
//        System.out.println("obj = " + obj);
//        }
//        catch(IOException ex) {
//            System.out.println("stream = " + stream.toString());
//        }

        String counter = "10";
        
        long parseLong = Long.parseLong(counter);
        
        int parseInt = (int) parseLong;
        
        System.out.println("parseLong = " + parseLong);
    }
}
