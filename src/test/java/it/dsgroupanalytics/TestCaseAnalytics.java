/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.dsgroupanalytics;

import it.dsgroup.analytics.ManageDataAnalytics;
import org.junit.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author matteos
 */
public class TestCaseAnalytics {
    
    @Test
    public void test_manageDataAnalytics() {
        String jsonString = "{\n" +
"  \"kind\": \"analytics#gaData\",\n" +
"  \"id\": \"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:177921861&dimensions=ga:userType,ga:socialNetwork&metrics=ga:sessions,ga:bounces,ga:goalCompletionsAll&start-date=2021-01-01&end-date=2021-04-30\",\n" +
"  \"query\": {\n" +
"    \"start-date\": \"2021-01-01\",\n" +
"    \"end-date\": \"2021-04-30\",\n" +
"    \"ids\": \"ga:177921861\",\n" +
"    \"dimensions\": \"ga:userType,ga:socialNetwork\",\n" +
"    \"metrics\": [\n" +
"      \"ga:sessions\",\n" +
"      \"ga:bounces\",\n" +
"      \"ga:goalCompletionsAll\"\n" +
"    ],\n" +
"    \"start-index\": 1,\n" +
"    \"max-results\": 1000\n" +
"  },\n" +
"  \"itemsPerPage\": 1000,\n" +
"  \"totalResults\": 7,\n" +
"  \"selfLink\": \"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:177921861&dimensions=ga:userType,ga:socialNetwork&metrics=ga:sessions,ga:bounces,ga:goalCompletionsAll&start-date=2021-01-01&end-date=2021-04-30\",\n" +
"  \"profileInfo\": {\n" +
"    \"profileId\": \"177921861\",\n" +
"    \"accountId\": \"121687582\",\n" +
"    \"webPropertyId\": \"UA-121687582-1\",\n" +
"    \"internalWebPropertyId\": \"179678418\",\n" +
"    \"profileName\": \"Tutti i dati del sito web\",\n" +
"    \"tableId\": \"ga:177921861\"\n" +
"  },\n" +
"  \"containsSampledData\": false,\n" +
"  \"columnHeaders\": [\n" +
"    {\n" +
"      \"name\": \"ga:userType\",\n" +
"      \"columnType\": \"DIMENSION\",\n" +
"      \"dataType\": \"STRING\"\n" +
"    },\n" +
"    {\n" +
"      \"name\": \"ga:socialNetwork\",\n" +
"      \"columnType\": \"DIMENSION\",\n" +
"      \"dataType\": \"STRING\"\n" +
"    },\n" +
"    {\n" +
"      \"name\": \"ga:sessions\",\n" +
"      \"columnType\": \"METRIC\",\n" +
"      \"dataType\": \"INTEGER\"\n" +
"    },\n" +
"    {\n" +
"      \"name\": \"ga:bounces\",\n" +
"      \"columnType\": \"METRIC\",\n" +
"      \"dataType\": \"INTEGER\"\n" +
"    },\n" +
"    {\n" +
"      \"name\": \"ga:goalCompletionsAll\",\n" +
"      \"columnType\": \"METRIC\",\n" +
"      \"dataType\": \"INTEGER\"\n" +
"    }\n" +
"  ],\n" +
"  \"totalsForAllResults\": {\n" +
"    \"ga:sessions\": \"1647\",\n" +
"    \"ga:bounces\": \"1323\",\n" +
"    \"ga:goalCompletionsAll\": \"0\"\n" +
"  },\n" +
"  \"rows\": [\n" +
"    [\n" +
"      \"New Visitor\",\n" +
"      \"(not set)\",\n" +
"      \"989\",\n" +
"      \"810\",\n" +
"      \"0\"\n" +
"    ],\n" +
"    [\n" +
"      \"New Visitor\",\n" +
"      \"Facebook\",\n" +
"      \"321\",\n" +
"      \"293\",\n" +
"      \"0\"\n" +
"    ],\n" +
"    [\n" +
"      \"New Visitor\",\n" +
"      \"Instagram\",\n" +
"      \"1\",\n" +
"      \"0\",\n" +
"      \"0\"\n" +
"    ],\n" +
"    [\n" +
"      \"New Visitor\",\n" +
"      \"Twitter\",\n" +
"      \"29\",\n" +
"      \"27\",\n" +
"      \"0\"\n" +
"    ],\n" +
"    [\n" +
"      \"Returning Visitor\",\n" +
"      \"(not set)\",\n" +
"      \"187\",\n" +
"      \"116\",\n" +
"      \"0\"\n" +
"    ],\n" +
"    [\n" +
"      \"Returning Visitor\",\n" +
"      \"Facebook\",\n" +
"      \"114\",\n" +
"      \"71\",\n" +
"      \"0\"\n" +
"    ],\n" +
"    [\n" +
"      \"Returning Visitor\",\n" +
"      \"Twitter\",\n" +
"      \"6\",\n" +
"      \"6\",\n" +
"      \"0\"\n" +
"    ]\n" +
"  ]\n" +
"}";
        
        
        ManageDataAnalytics.readDocumentAndJsonFormat(jsonString);
    }

    @Test
    public void testCreateDate() throws ParseException {

        ManageDataAnalytics.createDate();

    }

    @Test
    public void testManagedataImpression() {

/*        String jsonString = "{\n" +
                "    \"kind\": \"analytics#gaData\",\n" +
                "    \"id\": \"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:177921861&dimensions=ga:date,ga:campaign&metrics=ga:impressions&start-date=2021-01-01&end-date=2021-04-30\",\n" +
                "    \"query\": {\n" +
                "        \"start-date\": \"2021-01-01\",\n" +
                "        \"end-date\": \"2021-04-30\",\n" +
                "        \"ids\": \"ga:177921861\",\n" +
                "        \"dimensions\": \"ga:date,ga:campaign\",\n" +
                "        \"metrics\": [\n" +
                "            \"ga:impressions\"\n" +
                "        ],\n" +
                "        \"start-index\": 1,\n" +
                "        \"max-results\": 1000\n" +
                "    },\n" +
                "    \"itemsPerPage\": 1000,\n" +
                "    \"totalResults\": 0,\n" +
                "    \"selfLink\": \"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:177921861&dimensions=ga:date,ga:campaign&metrics=ga:impressions&start-date=2021-01-01&end-date=2021-04-30\",\n" +
                "    \"profileInfo\": {\n" +
                "        \"profileId\": \"177921861\",\n" +
                "        \"accountId\": \"121687582\",\n" +
                "        \"webPropertyId\": \"UA-121687582-1\",\n" +
                "        \"internalWebPropertyId\": \"179678418\",\n" +
                "        \"profileName\": \"Tutti i dati del sito web\",\n" +
                "        \"tableId\": \"ga:177921861\"\n" +
                "    },\n" +
                "    \"containsSampledData\": false,\n" +
                "    \"columnHeaders\": [\n" +
                "        {\n" +
                "            \"name\": \"ga:date\",\n" +
                "            \"columnType\": \"DIMENSION\",\n" +
                "            \"dataType\": \"STRING\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"ga:campaign\",\n" +
                "            \"columnType\": \"DIMENSION\",\n" +
                "            \"dataType\": \"STRING\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"ga:impressions\",\n" +
                "            \"columnType\": \"METRIC\",\n" +
                "            \"dataType\": \"INTEGER\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"totalsForAllResults\": {\n" +
                "        \"ga:impressions\": \"0\"\n" +
                //"        \"ga:campaign\": \"pippo\",\n" +
                //"        \"ga:date\": \"20210505\"\n" +
                "    }\n" +
                "}\n";*/

/*        String jsonString = "{\n" +
                "    \"kind\": \"analytics#gaData\",\n" +
                "    \"id\": \"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:177921861&dimensions=ga:date,ga:campaign&metrics=ga:impressions&start-date=2021-01-01&end-date=2021-04-30\",\n" +
                "    \"query\": {\n" +
                "        \"start-date\": \"2021-01-01\",\n" +
                "        \"end-date\": \"2021-04-30\",\n" +
                "        \"ids\": \"ga:177921861\",\n" +
                "        \"dimensions\": \"ga:date,ga:campaign\",\n" +
                "        \"metrics\": [\n" +
                "            \"ga:impressions\"\n" +
                "        ],\n" +
                "        \"start-index\": 1,\n" +
                "        \"max-results\": 1000\n" +
                "    },\n" +
                "    \"itemsPerPage\": 1000,\n" +
                "    \"totalResults\": 0,\n" +
                "    \"selfLink\": \"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:177921861&dimensions=ga:date,ga:campaign&metrics=ga:impressions&start-date=2021-01-01&end-date=2021-04-30\",\n" +
                "    \"profileInfo\": {\n" +
                "        \"profileId\": \"177921861\",\n" +
                "        \"accountId\": \"121687582\",\n" +
                "        \"webPropertyId\": \"UA-121687582-1\",\n" +
                "        \"internalWebPropertyId\": \"179678418\",\n" +
                "        \"profileName\": \"Tutti i dati del sito web\",\n" +
                "        \"tableId\": \"ga:177921861\"\n" +
                "    },\n" +
                "    \"containsSampledData\": false,\n" +
                "    \"columnHeaders\": [\n" +
                "        {\n" +
                "            \"name\": \"ga:date\",\n" +
                "            \"columnType\": \"DIMENSION\",\n" +
                "            \"dataType\": \"STRING\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"ga:campaign\",\n" +
                "            \"columnType\": \"DIMENSION\",\n" +
                "            \"dataType\": \"STRING\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"ga:impressions\",\n" +
                "            \"columnType\": \"METRIC\",\n" +
                "            \"dataType\": \"INTEGER\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"totalsForAllResults\": {\n" +
                "        \"ga:impressions\": \"0\"\n" +
                "    },\n" +
                "\t\"rows\" : [\n" +
                "\t\t[\n" +
                "\t\t\t\"20210505\",\n" +
                "\t\t\t\"pippo_campaign\",\n" +
                "\t\t\t\"10\"\n" +
                "\t\t],\n" +
                "\t\t[\n" +
                "\t\t\t\"20210505\",\n" +
                "\t\t\t\"pluto_campaign\",\n" +
                "\t\t\t\"6\"\n" +
                "\t\t]\n" +
                "\t]\n" +
                "}";*/

        String jsonString = "{\n" +
                "\t\"kind\": \"analytics#gaData\",\n" +
                "\t\"id\": \"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:177921861&dimensions=ga:date,ga:campaign,ga:campaignCode&metrics=ga:impressions,ga:users,ga:newUsers&start-date=2021-01-01&end-date=2021-04-30\",\n" +
                "\t\"query\": {\n" +
                "\t\t\"start-date\": \"2021-01-01\",\n" +
                "\t\t\"end-date\": \"2021-04-30\",\n" +
                "\t\t\"ids\": \"ga:177921861\",\n" +
                "\t\t\"dimensions\": \"ga:date,ga:campaign,ga:campaignCode\",\n" +
                "\t\t\"metrics\": [\n" +
                "\t\t\t\"ga:impressions\",\n" +
                "\t\t\t\"ga:users\",\n" +
                "\t\t\t\"ga:newUsers\"\n" +
                "\t\t],\n" +
                "\t\t\"start-index\": 1,\n" +
                "\t\t\"max-results\": 1000\n" +
                "\t},\n" +
                "\t\"itemsPerPage\": 1000,\n" +
                "\t\"totalResults\": 0,\n" +
                "\t\"selfLink\": \"https://www.googleapis.com/analytics/v3/data/ga?ids=ga:177921861&dimensions=ga:date,ga:campaign,ga:campaignCode&metrics=ga:impressions,ga:users,ga:newUsers&start-date=2021-01-01&end-date=2021-04-30\",\n" +
                "\t\"profileInfo\": {\n" +
                "\t\t\"profileId\": \"177921861\",\n" +
                "\t\t\"accountId\": \"121687582\",\n" +
                "\t\t\"webPropertyId\": \"UA-121687582-1\",\n" +
                "\t\t\"internalWebPropertyId\": \"179678418\",\n" +
                "\t\t\"profileName\": \"Tutti i dati del sito web\",\n" +
                "\t\t\"tableId\": \"ga:177921861\"\n" +
                "\t},\n" +
                "\t\"containsSampledData\": false,\n" +
                "\t\"columnHeaders\": [\n" +
                "\t\t{\n" +
                "\t\t\t\"name\": \"ga:date\",\n" +
                "\t\t\t\"columnType\": \"DIMENSION\",\n" +
                "\t\t\t\"dataType\": \"STRING\"\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"name\": \"ga:campaign\",\n" +
                "\t\t\t\"columnType\": \"DIMENSION\",\n" +
                "\t\t\t\"dataType\": \"STRING\"\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"name\": \"ga:campaignCode\",\n" +
                "\t\t\t\"columnType\": \"DIMENSION\",\n" +
                "\t\t\t\"dataType\": \"STRING\"\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"name\": \"ga:impressions\",\n" +
                "\t\t\t\"columnType\": \"METRIC\",\n" +
                "\t\t\t\"dataType\": \"INTEGER\"\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"name\": \"ga:users\",\n" +
                "\t\t\t\"columnType\": \"METRIC\",\n" +
                "\t\t\t\"dataType\": \"INTEGER\"\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"name\": \"ga:newUsers\",\n" +
                "\t\t\t\"columnType\": \"METRIC\",\n" +
                "\t\t\t\"dataType\": \"INTEGER\"\n" +
                "\t\t}\n" +
                "\t],\n" +
                "\t\"totalsForAllResults\": {\n" +
                "\t\t\"ga:impressions\": \"0\",\n" +
                "\t\t\"ga:users\": \"0\",\n" +
                "\t\t\"ga:newUsers\": \"0\"\n" +
                "\t}\n" +
                "}";

        Map<String, ArrayList<Map<String, Map<String, String>>>> stringArrayListMap = ManageDataAnalytics.gestioneDataImpressions(jsonString);
        System.out.println("stringArrayListMap= " + stringArrayListMap);


    }

    @Test
    public void testSetDataDate() throws ParseException {

        ManageDataAnalytics.setDataDate();

    }

}
